## 1-5 常用API### 第二部分
### 第1节 Object类
#### Object类的toString方法
#### Object类的equals方法
#### 重写Object类的equals方法
#### Objects类的equals方法
### 第2节 Date类
#### 毫秒值的概念和作用
#### Date类的构造方法和成员方法
#### DateFormat类&SimpleDateFormat类介绍
#### DateFormat类的format方法和parse方法
#### 练习_计算出一个人已经出生了多少天
### 第3节 Calendar类
#### Calendar类介绍_获取对象的方式
#### Calendar类的常用成员方法
### 第4节 System类
#### System类的常用方法
### 第5节 StringBuilder类
#### StringBuilder的原理
#### StringBuilder的构造方法和append方法
#### StringBuilder的toString方法
### 第6节 基本类型包装类
#### 包装类的概念
#### 包装类_装箱与拆箱
#### 包装类_自动装箱与自动拆箱
#### 包装类_基本类型与字符串类型之

