package com.weieast.myjavalife.day01;

//这是给人看的注释,对程序运行不产生任何影响
//这是第二行注释
//以上为单行注释
//public class 用来修饰一个类
public class HelloWorld {
	/*
	多行注释
	*/
	//代表程序执行的入口
	public static void main (String[] args){
	//表示在命令行显示括号的中的内容(显示字符串需要用双引号包起来)
	System.out.println("hello, world");
	}
}
