package com.weieast.myjavalife;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyjavalifeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyjavalifeApplication.class, args);
	}

}
